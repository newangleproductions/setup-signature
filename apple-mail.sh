#!/bin/bash

check_input() {
  echo "enter name:"
	exec 3<>/dev/tty
	read -u 3 SIG_NAME

	if [[ -z "$SIG_NAME" ]]; then
		echo "missing name"
		exit 1
	fi

  echo "enter job title:"
	exec 3<>/dev/tty
	read -u 3 SIG_JOB_TITLE

	if [[ -z "$SIG_JOB_TITLE" ]]; then
		echo "missing job title"
		exit 1
	fi

  echo "done."
}

load_signature_file() {
	echo "searching signature file..."

	local SIGNATURES_PATH_ICLOUD_V6="$HOME/Library/Mobile Documents/com~apple~mail/Data/V6/Signatures/"
    local SIGNATURES_PATH_ICLOUD_V5="$HOME/Library/Mobile Documents/com~apple~mail/Data/V5/Signatures/"
	local SIGNATURES_PATH_ICLOUD_V4="$HOME/Library/Mobile Documents/com~apple~mail/Data/V4/Signatures/"
	local SIGNATURES_PATH_ICLOUD_V3="$HOME/Library/Mobile Documents/com~apple~mail/Data/V3/Signatures/"
	local SIGNATURES_PATH_ICLOUD_V2="$HOME/Library/Mobile Documents/com~apple~mail/Data/V2/Signatures/"
	local SIGNATURES_PATH_NON_ICLOUD_V6="$HOME/Library/Mail/V6/MailData/Signatures/"
	local SIGNATURES_PATH_NON_ICLOUD_V5="$HOME/Library/Mail/V5/MailData/Signatures/"
	local SIGNATURES_PATH_NON_ICLOUD_V4="$HOME/Library/Mail/V4/MailData/Signatures/"
	local SIGNATURES_PATH_NON_ICLOUD_V3="$HOME/Library/Mail/V3/MailData/Signatures/"
	local SIGNATURES_PATH_NON_ICLOUD_V2="$HOME/Library/Mail/V2/MailData/Signatures/"

	if [ -d "$SIGNATURES_PATH_ICLOUD_V6" ]; then
		echo "found iCloud signature directory v6"
		local SIGNATURE_PATH="$SIGNATURES_PATH_ICLOUD_V6"
	elif [ -d "$SIGNATURES_PATH_ICLOUD_V5" ]; then
		echo "found iCloud signature directory v5"
		local SIGNATURE_PATH="$SIGNATURES_PATH_ICLOUD_V5"
	elif [ -d "$SIGNATURES_PATH_ICLOUD_V4" ]; then
		echo "found iCloud signature directory v4"
		local SIGNATURE_PATH="$SIGNATURES_PATH_ICLOUD_V4"
	elif [ -d "$SIGNATURES_PATH_ICLOUD_V3" ]; then
		echo "found iCloud signature directory v3"
		local SIGNATURE_PATH="$SIGNATURES_PATH_ICLOUD_V3"
	elif [ -d "$SIGNATURES_PATH_ICLOUD_V2" ]; then
		echo "found iCloud signature directory v2"
		local SIGNATURE_PATH="$SIGNATURES_PATH_ICLOUD_V2"
	elif [ -d "$SIGNATURES_PATH_NON_ICLOUD_V6" ]; then
		echo "found non-iCloud signature directory v6"
		local SIGNATURE_PATH="$SIGNATURES_PATH_NON_ICLOUD_V6"
	elif [ -d "$SIGNATURES_PATH_NON_ICLOUD_V5" ]; then
		echo "found non-iCloud signature directory v5"
		local SIGNATURE_PATH="$SIGNATURES_PATH_NON_ICLOUD_V5"
	elif [ -d "$SIGNATURES_PATH_NON_ICLOUD_V4" ]; then
		echo "found non-iCloud signature directory v4"
		local SIGNATURE_PATH="$SIGNATURES_PATH_NON_ICLOUD_V4"
	elif [ -d "$SIGNATURES_PATH_NON_ICLOUD_V3" ]; then
		echo "found non-iCloud signature directory v3"
		local SIGNATURE_PATH="$SIGNATURES_PATH_NON_ICLOUD_V3"
	elif [ -d "$SIGNATURES_PATH_NON_ICLOUD_V2" ]; then
		echo "found non-iCloud signature directory v2"
		local SIGNATURE_PATH="$SIGNATURES_PATH_NON_ICLOUD_V2"
	else
		echo "signature directory not found"
		exit 1
	fi

	SIGNATURE_FILE=`ls -1t "$SIGNATURE_PATH"*.mailsignature | head -n 1`

	if [[ $? -ne 0 ]] || [[ -z "$SIGNATURE_FILE" ]]; then
		echo "listing error, create a signature"
		exit 1
	fi
}

download_signature() {
	echo "downloading signature..."
  
  SIGNATURE_URL="https://bitbucket.org/newangleproductions/setup-signature/raw/fb70a77f16de6ca5cb3c22717ef4cd5169f4dcbd/signature.html"
	SIGNATURE_HTML=`curl -s "$SIGNATURE_URL"`

	if [[ -z "$SIGNATURE_HTML" ]]; then
		echo "empty signature url response"
		exit 1
	fi

	echo "done."
}

setup_signature() {
	echo "setting up signature..."

	i=0;
	SIGNATURE=""
	while read line; do
		if [[ -z "$line" ]]; then
			break
		fi

		if [[ "$i" -eq 0 ]]; then
			SIGNATURE="$line"
		else
			SIGNATURE="$SIGNATURE\n$line"
		fi

		i=$((i + 1))
	done < "$SIGNATURE_FILE"

  UPDATED_SIGNATURE_HTML=${SIGNATURE_HTML/__NAME__/$SIG_NAME}
  UPDATED_SIGNATURE_HTML=${UPDATED_SIGNATURE_HTML/__TITLE__/$SIG_JOB_TITLE}

	SIGNATURE="$SIGNATURE\n\n$UPDATED_SIGNATURE_HTML"

	chflags nouchg "$SIGNATURE_FILE"
	echo -e "$SIGNATURE" > "$SIGNATURE_FILE"
	chflags uchg "$SIGNATURE_FILE"

	echo "done."
}

check_input
load_signature_file
download_signature
setup_signature